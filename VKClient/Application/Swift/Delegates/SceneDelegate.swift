import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    var splashScreenWindow: UIWindow?
    var currentWindowScene: UIWindowScene?
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    let vkDefaults = VKDefaults.shared

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        var rootViewController: UIViewController!
        
        let tokenDidNotExpired = DateComparer.isActive(vkDefaults.expirationDate) ?? true
        
        if vkDefaults.paramsDidFetched && tokenDidNotExpired {
            rootViewController = RootTabBarController()
        } else {
            rootViewController = StartViewController()
        }
        
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        self.currentWindowScene = windowScene
        
        window = UIWindow(frame: windowScene.coordinateSpace.bounds)
        appDelegate.window = window
        window?.windowScene = windowScene
        window?.rootViewController = rootViewController
        window?.backgroundColor = UIColor.white
        window?.makeKeyAndVisible()
        
        showSplashScreen()
        observeTokenFetching()
        observeLogout()
    }

    func sceneDidDisconnect(_ scene: UIScene) {
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
    }

    func sceneWillResignActive(_ scene: UIScene) {
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
    }

    private func showSplashScreen() {
        guard let windowScene = currentWindowScene else { return }
        splashScreenWindow = UIWindow(frame: windowScene.coordinateSpace.bounds)
        let splashScreenViewController = SplashScreenViewController()
        
        splashScreenWindow?.windowScene = windowScene
        splashScreenWindow?.windowLevel = .normal + 1
        splashScreenWindow?.rootViewController = splashScreenViewController
        splashScreenWindow?.backgroundColor = UIColor.clear
        splashScreenWindow?.isHidden = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(hideSplashScreen), name: Notification.Name.canHideSplashScreen, object: nil)
    }
    
    private func observeTokenFetching() {
        NotificationCenter.default.addObserver(self, selector: #selector(runClient), name: Notification.Name.tokenDidFetched, object: nil)
    }
    
    private func observeLogout() {
        NotificationCenter.default.addObserver(self, selector: #selector(showStartScreen), name: Notification.Name.logout, object: nil)
    }
    
    @objc private func showStartScreen() {
        window?.rootViewController = nil
        window?.rootViewController = StartViewController()
    }
    
    @objc private func runClient() {
        window?.rootViewController = nil
        window?.rootViewController = RootTabBarController()
    }
    
    @objc private func hideSplashScreen() {
        splashScreenWindow = nil
    }
}

