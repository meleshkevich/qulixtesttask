import Foundation

enum GetWallParametrs {
    static let owner_id = "owner_id"
    static let domain   = "domain"
    static let offset   = "offset"
    static let count    = "count"
    static let filter   = "filter"
    static let extended = "extended"
    static let fields   = "fields"
}
