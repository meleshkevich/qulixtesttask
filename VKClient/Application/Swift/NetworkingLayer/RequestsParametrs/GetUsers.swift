enum GetUsersParametrs: String {
    case usersIDS  = "user_ids"
    case fields    = "fields"
    case nameCase = "name_case"
}

// MARK: Падежи для запроса
enum NameCases {
    static let nom = "nom"
    static let get = "gen"
    static let dat = "dat"
    static let acc = "acc"
    static let ins = "ins"
    static let abl = "abl"
}
