import Foundation

struct VKBasicConstants {
    static let appID = 7643442
    static let apiVersion = 5.124
    static let logoutUrl = "http://api.vk.com/oauth/logout"
}

enum APIMethods: String {
    case getUsers    = "users.get?"
    case getWall     = "wall.get?"
}

protocol EndpointType {
    var methodName: APIMethods { get }
    var methodOptionalParametrs: [String: String]? { get }
}

struct Endpoint: EndpointType {
    let secureTransferProtocol: String = "https://"
    let vkAPIName: String              = "api.vk.com/method/"
    let apiVersion                     = "\(VKBasicConstants.apiVersion)"
    
    var basicStringURL: String {
        return secureTransferProtocol + vkAPIName
    }
    
    var methodName: APIMethods
    var methodOptionalParametrs: [String: String]?
    var accessToken: String
    
    func getEndpointURL() -> URL? {
        let startStringURL = basicStringURL + methodName.rawValue
        var parametrs: String!
        
        if let additionalParametrs = methodOptionalParametrs {
            parametrs = processParametrs(additionalParametrs, ["access_token": accessToken], ["v": apiVersion])
        } else {
            parametrs = processParametrs(["access_token": accessToken], ["v": apiVersion])
        }
        
        return URL(string: startStringURL + parametrs)
    }
    
    func processParametrs(_ params: [String: String]...) -> String {
        var processedParametrs = ""
        let paramStartSymbol = "&"
        
        for paramDictionary in params {
            for (key, value) in paramDictionary {
                processedParametrs.append(paramStartSymbol)
                processedParametrs.append("\(key)=\(value)")
            }
        }
        
        return processedParametrs
    }
}
