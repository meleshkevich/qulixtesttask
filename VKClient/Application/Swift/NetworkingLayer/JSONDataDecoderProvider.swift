import Foundation

struct VKAPIArrayShell<ObjectModel: Codable>: Codable {
    let response: [ObjectModel]
}

struct VKAPIObjectShell<ObjectModel: Codable>: Codable {
    let response: ObjectModel
}

class JSONDataDecoderProvider<Type: Codable> {
    func decodeArrayShell(data: Data) -> (VKAPIArrayShell<Type>)? {
        let decoder = JSONDecoder()
        
        do {
            let decodedJson = try decoder.decode(VKAPIArrayShell<Type>.self, from: data)
            return decodedJson
        } catch {
            print("Decoding error")
            print(error.localizedDescription)
        }
        
        return nil
    }
    
    func decodeObjectShell(data: Data) -> (VKAPIObjectShell<Type>)? {
        let decoder = JSONDecoder()
        
        do {
            let decodedJson = try decoder.decode(VKAPIObjectShell<Type>.self, from: data)
            return decodedJson
        } catch {
            print("Decoding error")
            print(error.localizedDescription)
        }
        
        return nil
    }
}
