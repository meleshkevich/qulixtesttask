import Foundation
import Alamofire

class NetworkingManager {
    static let shared = NetworkingManager()
    
    private init() {}
    
    private let vkDefaults = VKDefaults.shared
    
    private var dataRequests: [String: DataRequest] = [:]
    
    func getUser(compelition: @escaping (UserModel?, Error?) -> ()) {
        let additionalParametrs = [
            GetUsersParametrs.usersIDS.rawValue: vkDefaults.userID,
            GetUsersParametrs.fields.rawValue: "photo_100",
            GetUsersParametrs.nameCase.rawValue: NameCases.nom]
        
        let endpoint = Endpoint(methodName: .getUsers, methodOptionalParametrs: additionalParametrs, accessToken: vkDefaults.accessToken)
        guard let url = endpoint.getEndpointURL() else { return }
        
        AF.request(url, method: .get).response(queue: .main) { (response) in
            switch response.result {
            case .success(let data):
                guard let unwrappedData = data else { compelition(nil, nil); return }
                let decoderProvider = JSONDataDecoderProvider<UserModel>()
                guard let shellWithModel = decoderProvider.decodeArrayShell(data: unwrappedData) else { compelition(nil, nil); return }
                compelition(shellWithModel.response[0], nil)
            case .failure(let error):
                compelition(nil, error)
            }
        }
    }
    
    func getWallPosts(offset: Int, type: WallPostsType, compelition: @escaping (PostsModel?, Error?) -> ()) {
        let additionalParametrs: [String: String] = [
            GetWallParametrs.owner_id: vkDefaults.userID,
            GetWallParametrs.count: String(10),
            GetWallParametrs.extended: String(1),
            GetWallParametrs.filter: type.rawValue,
            GetWallParametrs.offset: String(offset)
        ]
        
        let endpoint = Endpoint(methodName: .getWall, methodOptionalParametrs: additionalParametrs, accessToken: vkDefaults.accessToken)
        guard let url = endpoint.getEndpointURL() else { return }
        
        AF.request(url, method: .get).response { response in
            switch response.result {
            case .success(let data):
                guard let unwrappedData = data else { compelition(nil, nil); return }
                let decoderProvider = JSONDataDecoderProvider<PostsModel>()
                guard let shellWithModel = decoderProvider.decodeObjectShell(data: unwrappedData) else { compelition(nil, nil); return }
                compelition(shellWithModel.response, nil)
            case .failure(let error):
                compelition(nil, error)
            }
        }
    }
    
    func cancelDataRequestFor(key: String) {
        guard let dataRequest = dataRequests[key] else { return }
        dataRequest.cancel()
        dataRequests.removeValue(forKey: key)
    }
    
    func getPicture(by urlString: String, compelition: @escaping (Data?, Error?) -> ()) {
        let dataRequest = AF.request(urlString, method: .get).response { [weak self] response in
            defer {
                self?.dataRequests.removeValue(forKey: urlString)
            }
            
            switch response.result {
            case .success(let imageData):
                compelition(imageData, nil)
            case .failure(let error):
                compelition(nil, error)
            }
        }
        
        dataRequests.updateValue(dataRequest, forKey: urlString)
    }
    
    func logout() {
        AF.request(VKBasicConstants.logoutUrl).response { (response) in
            
            switch response.result {
            case .success(let data):
                dprint(data as Any)
            case .failure(let error):
                dprint(error)
            }
        }
    }
}
