import Foundation

typealias YearMonthDayHourMinuteSecond = (Int, Int, Int, Int, Int, Int)
typealias YearMonthDayHourMinute = (Int, Int, Int, Int, Int)
typealias YearMonthDay = (Int, Int, Int)

struct Month {
    static let monthArray = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sept",
        "Oct",
        "Nov",
        "Dec"
    ]
    
    static func getMonth(_ number: Int) -> String {
        return monthArray[number - 1]
    }
}

class DateComparer {
    
    public static let currentDate: YearMonthDayHourMinute! = {
        let date = Date()
        let components = date.get(.day, .month, .year, .hour, .minute)
        
        if let day = components.day, let month = components.month, let year = components.year, let hour = components.hour, let minute = components.minute {
            return (year, month, day, hour, minute)
        }
        
        return nil
    }()
    
    public static func parseDateToSecond(_ dateString: String) -> YearMonthDayHourMinuteSecond? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm"
        
        guard let date = formatter.date(from: dateString) else { return nil }
        
        formatter.dateFormat = "yyyy"
        guard let year = Int(formatter.string(from: date)) else { return nil }
        formatter.dateFormat = "MM"
        guard let month = Int(formatter.string(from: date)) else { return nil }
        formatter.dateFormat = "dd"
        guard let day = Int(formatter.string(from: date)) else { return nil }
        formatter.dateFormat = "HH"
        guard let hour = Int(formatter.string(from: date)) else { return nil }
        formatter.dateFormat = "mm"
        guard let minute = Int(formatter.string(from: date)) else { return nil }
        formatter.dateFormat = "ss"
        guard let second = Int(formatter.string(from: date)) else { return nil }
        
        return (year, month, day, hour, minute, second)
    }
    
    public static func parseDateToMinute(_ dateString: String) -> YearMonthDayHourMinute? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm"
        
        guard let date = formatter.date(from: dateString) else { return nil }
        
        formatter.dateFormat = "yyyy"
        guard let year = Int(formatter.string(from: date)) else { return nil }
        formatter.dateFormat = "MM"
        guard let month = Int(formatter.string(from: date)) else { return nil }
        formatter.dateFormat = "dd"
        guard let day = Int(formatter.string(from: date)) else { return nil }
        formatter.dateFormat = "HH"
        guard let hour = Int(formatter.string(from: date)) else { return nil }
        formatter.dateFormat = "mm"
        guard let minute = Int(formatter.string(from: date)) else { return nil }
        
        return (year, month, day, hour, minute)
    }
    
    public static func parseDateFrom(seconds: Int) -> Date? {
        return Date(timeIntervalSince1970: TimeInterval(seconds))
    }
    
    public static func isActive(_ endDate: String) -> Bool? {
        guard let currentDate = currentDate else { return nil }
        guard let compareDate = parseDateToMinute(endDate) else { return nil }
        
        return dateInFuture(compareDate, currentDate)
    }
    
    private static func dateInFuture(_ dateToCompare: YearMonthDayHourMinute, _ currentDate: YearMonthDayHourMinute) -> Bool {
        if dateToCompare < currentDate {
            return false
        } else if dateToCompare == currentDate {
            return true
        } else if dateToCompare > currentDate {
            return true
        }
        
        return false
    }
}
