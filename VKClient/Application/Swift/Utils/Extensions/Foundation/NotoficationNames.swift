import Foundation

extension Notification.Name {
    static let canHideSplashScreen = Notification.Name("canHideSplashScreen")
    static let tokenDidFetched     = Notification.Name("tokenDidFetched")
    static let logout              = Notification.Name("logout")
}
