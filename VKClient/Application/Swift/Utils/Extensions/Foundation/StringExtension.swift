enum DeletingError: Error {
    case deletingPrefix(error: String)
    case deletingSufix(error: String)
}

extension String {
    func deletePrefix(_ prefix: String) throws -> String {
        guard self.hasPrefix(prefix) else { throw DeletingError.deletingPrefix(error: "No such prefix")}
        return String(self.dropFirst(prefix.count))
    }
    
    func deleteSuffix(_ suffix: String) throws -> String {
        guard self.hasPrefix(suffix) else { throw DeletingError.deletingSufix(error: "No such sufix")}
        return String(self.dropLast(suffix.count))
    }
}
