import Foundation

extension Date {
    func get(_ components: Calendar.Component..., calendar: Calendar = Calendar.current) -> DateComponents {
        return calendar.dateComponents(Set(components), from: self)
    }

    func get(_ component: Calendar.Component, calendar: Calendar = Calendar.current) -> Int {
        return calendar.component(component, from: self)
    }
}

extension Date {
    
    var currentYear: Int {
        get {
            return get(.year)
        }
    }
    
    var currentMonth: Int {
        get {
            return get(.month)
        }
    }
    
    var currentDay: Int {
        get {
            return get(.day)
        }
    }
    
    var currentHour: Int {
        get {
            return get(.hour)
        }
    }
    
    var currentMinute: Int {
        get {
            return get(.minute)
        }
    }
    
    var currentSecond: Int {
        get {
            return get(.minute)
        }
    }
}
