import UIKit

extension UIView {
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set(newValue) {
            layer.cornerRadius = CGFloat(newValue)
        }
    }
    
    func pinSubviewAnchors(add: Bool, subview: UIView) {
        if add {
            addSubview(subview)
        }
        
        subview.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            subview.topAnchor.constraint(equalTo: topAnchor, constant: 0),
            subview.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0),
            subview.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            subview.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0)
        ])
    }
}

extension UIView {
    var minXFrame: CGFloat {
        return frame.minX
    }

    var midXFrame: CGFloat {
        return frame.midX
    }
    
    var maxXFrame: CGFloat {
        return frame.maxX
    }
    
    var minYFrame: CGFloat {
        return frame.minY
    }
    
    var midYFrame: CGFloat {
        return frame.midY
    }
    
    var maxYFrame: CGFloat {
        return frame.maxY
    }
}
