import UIKit

class ImagesCacher {
    var cache = NSCache<NSString, UIImage>()
    
    static let shared = ImagesCacher()
    private init() {}
    
    func checkCache(for key: String) -> UIImage? {
        guard let image = cache.object(forKey: key as NSString) else { return nil }
        return image
    }
}

