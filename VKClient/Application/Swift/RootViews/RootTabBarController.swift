import UIKit

class RootTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupController()
    }
    
    private func setupController() {
        let postsViewController = RootNavigationController(rootViewController: PostsViewController())
        let settingsViewController = RootNavigationController(rootViewController: SettingsViewController())
        
        postsViewController.title = "Posts"
        settingsViewController.title = "Settings"
        
        postsViewController.tabBarItem.image = UIImage(systemName: "text.bubble")
        settingsViewController.tabBarItem.image = UIImage(systemName: "person.circle.fill")
        
        tabBar.tintColor = UIConstants.vkUIPatternColor
        
        setViewControllers([settingsViewController, postsViewController], animated: false)
    }
}
