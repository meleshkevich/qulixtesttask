import UIKit

class StartViewController: UIViewController {
    
    private let appIconImageView = UIImageView(image: UIImage(named: "VKRoundedIcon"))
    private let loginButton = UIButton(type: .roundedRect)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI()
    }
    
    private func setUI() {
        addAppIconImageView()
        configureLoginButton()
    }
    
    private func addAppIconImageView() {
        view.addSubview(appIconImageView)
        
        appIconImageView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            appIconImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            appIconImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            appIconImageView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.2),
            appIconImageView.heightAnchor.constraint(equalTo: appIconImageView.widthAnchor)
        ])
    }
    
    private func configureLoginButton() {
        let loginButtonHeight: CGFloat = 60.0
        
        loginButton.setTitle("Log In", for: .normal)
        loginButton.setTitleColor(UIColor.white, for: .normal)
        loginButton.backgroundColor = UIConstants.vkUIPatternColor
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        loginButton.cornerRadius = loginButtonHeight/10
        loginButton.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        
        loginButton.addTarget(self, action: #selector(loginButtonTapped), for: .touchUpInside)
        
        view.addSubview(loginButton)
        
        NSLayoutConstraint.activate([
            loginButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -20),
            loginButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            loginButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            loginButton.heightAnchor.constraint(equalToConstant: loginButtonHeight)
        ])
    }
    
    @objc private func loginButtonTapped() {
        let authorizationVC = AuthorizationViewController()
        authorizationVC.modalPresentationStyle = .fullScreen
        self.present(authorizationVC, animated: true, completion: nil)
    }
}
