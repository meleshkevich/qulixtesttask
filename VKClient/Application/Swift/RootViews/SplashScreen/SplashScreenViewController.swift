import UIKit

class SplashScreenViewController: UIViewController {
    
    private var appIconImageView = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.white
        addBackgroundLogo()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
            self?.animateDisappearence()
        }
    }
    
    private func addBackgroundLogo() {
        let logoImageView = UIImageView(image: UIImage(named: "VKRoundedIcon"))
        view.addSubview(logoImageView)
        
        logoImageView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            logoImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            logoImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            logoImageView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.2),
            logoImageView.heightAnchor.constraint(equalTo: logoImageView.widthAnchor)
        ])
    }
    
    private func animateDisappearence() {
        UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseOut], animations: { [weak self] in
            guard let unwrappedView = self?.view else { return }
            unwrappedView.transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
        }) { (_) in
            NotificationCenter.default.post(Notification(name: Notification.Name.canHideSplashScreen))
        }
    }
}
