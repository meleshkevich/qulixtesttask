import UIKit

struct UIConstants {
    static let vkUIPatternColor: UIColor = RGBConverter.getColorFrom(82, 128, 182)
    static let vkCorePatternColor: CGColor = RGBConverter.getCoreColorFrom(82, 128, 182)
}
