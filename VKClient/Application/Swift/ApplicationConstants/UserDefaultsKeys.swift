import Foundation

enum UserDefaultsKeys: String {
    case accessToken    = "AccessToken"
    case userID         = "UserID"
    case expirationDate = "ExpirationDate"
}
