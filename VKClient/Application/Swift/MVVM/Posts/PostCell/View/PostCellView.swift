import UIKit

class PostTableViewCell: UITableViewCell {
    
    @IBOutlet weak var placeholderView: UIView!
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var repostsLabel: UILabel!
    @IBOutlet weak var postOwnerImageView: UIImageView!
    @IBOutlet weak var postOwnerNameLabel: UILabel!
    @IBOutlet weak var postDateLabel: UILabel!
    @IBOutlet weak var postTextLabel: UILabel!
    
    var viewModel: PostCellViewModel! {
        didSet {
            setupUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        postOwnerImageView.image = nil
        NetworkingManager.shared.cancelDataRequestFor(key: viewModel.ownerImageUrl)
    }
    
    private func setupUI() {
        likesLabel.text = String(viewModel.likes)
        commentsLabel.text = String(viewModel.comments)
        repostsLabel.text = String(viewModel.reposts)
        
        postOwnerNameLabel.text = viewModel.ownerName
        postTextLabel.text = viewModel.text
        
        
        if let image = ImagesCacher.shared.checkCache(for: viewModel.ownerImageUrl) {
            postOwnerImageView.image = image
        } else {
            NetworkingManager.shared.getPicture(by: viewModel.ownerImageUrl) { [weak self] (imageData, error) in
                guard error == nil else { return }
                guard let unwrappedData = imageData else { return }
                guard let uiImage = UIImage(data: unwrappedData) else { return }
                self?.postOwnerImageView.image = uiImage
                guard let key = self?.viewModel.ownerImageUrl as NSString? else { return }
                ImagesCacher.shared.cache.setObject(uiImage, forKey: key )
            }
        }
        
        guard let date = DateComparer.parseDateFrom(seconds: viewModel.date) else { return }
        postDateLabel.text = "\(date.currentDay) \(Month.getMonth(date.currentMonth)) \(date.currentYear) \(date.currentHour):\(date.currentMinute)"
    }
}
