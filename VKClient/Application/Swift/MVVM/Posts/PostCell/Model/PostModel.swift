import Foundation

struct PostModel: Codable {
    let from_id:      Int
    let date:         Int
    let text:         String
    let comments:     MediaNumberModel
    let likes:        MediaNumberModel
    let reposts:      MediaNumberModel
}

struct MediaNumberModel: Codable {
    let count: Int
}

struct WallProfileModel: Codable {
    let id:         Int
    let first_name: String
    let last_name:  String
    let photo_50:   String
}

struct GroupModel: Codable {
    let id:       Int
    let name:     String
    let photo_50: String
}
