import Foundation

class PostCellViewModel {
    var post: PostModel
    
    var date: Int
    var text: String
    var comments: Int
    var likes: Int
    var reposts: Int
    var ownerName: String
    var ownerImageUrl: String
    
    init(with post: PostModel, owner: OwnerModel) {
        self.post = post
        
        date = post.date
        text = post.text
        comments = post.comments.count
        likes = post.likes.count
        reposts = post.reposts.count
        ownerName = owner.name
        ownerImageUrl = owner.imageUrl
    }
}

