import Foundation

protocol PostsViewModelDelegate: class {
    func refreshTableView()
}

class PostsViewModel {
    
    private var selectedPostsType: WallPostsType!
    
    private var postsGroups: [PostsModel] = []
    
    private weak var delegate: PostsViewModelDelegate!
    
    private var posts: [PostModel] = []
    
    var cellsNumber: Int = 0
    var offset: Int = 0
    
    init(postsType: WallPostsType, delegate: PostsViewModelDelegate) {
        self.selectedPostsType = postsType
        self.delegate = delegate
        
        getData()
    }
    
    func getData() {
        NetworkingManager.shared.getWallPosts(offset: offset, type: selectedPostsType) { [weak self] (optionalPosts, error) in
            guard error == nil else { return }
            guard var posts = optionalPosts else { return }
            
            posts.items = posts.items.sorted(by: { (first, second) in
                return first.date > second.date
            })
            
            self?.postsGroups.append(posts)
            self?.posts += posts.items
            
            self?.cellsNumber = posts.items.count
            if !(posts.items.count < 10) {
                self?.offset += posts.items.count
            }
            self?.delegate.refreshTableView()
        }
    }
    
    func downloadNext() {
        NetworkingManager.shared.getWallPosts(offset: offset, type: selectedPostsType) { [weak self] (optionalPosts, error) in
            guard error == nil else { return }
            guard var posts = optionalPosts else { return }
            
            posts.items = posts.items.sorted(by: { (first, second) in
                return first.date > second.date
            })
            
            self?.postsGroups.append(posts)
            self?.posts += posts.items
            
            self?.cellsNumber += posts.items.count
            self?.offset += posts.items.count
            
            self?.delegate.refreshTableView()
        }
    }
    
    func refreshData() {
        removePostsData()
        
        getData()
    }
    
    func changePostsType() {
        removePostsData()
        selectedPostsType.changeState()
        
        getData()
    }
    
    func generatePostsCellViewModel(for index: Int) -> PostCellViewModel {
        let owner = generateOwnerInfo(by: posts[index].from_id)
        let ownerName = owner.0
        let ownerImageUrl = owner.1
        
        return PostCellViewModel(with: posts[index], owner: OwnerModel(name: ownerName, imageUrl: ownerImageUrl))
    }
    
    private func removePostsData() {
        cellsNumber = 0
        offset = 0
        postsGroups.removeAll()
    }
    
    private func generateOwnerInfo(by id: Int) -> (String, String) {
        for post in postsGroups {
            for profile in post.profiles {
                if id == profile.id {
                    let ownerName = profile.first_name + " " + profile.last_name
                    let ownerImageUrl = profile.photo_50
                    
                    return (ownerName, ownerImageUrl)
                }
            }
            
            for group in post.groups {
                if id == group.id {
                    let ownerName = group.name
                    let ownerImageUrl = group.photo_50
                    
                    return (ownerName, ownerImageUrl)
                }
            }
        }
        
        return ("", "")
    }
}
