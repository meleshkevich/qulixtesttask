import UIKit

enum WallPostsType: String {
    case all = "all"
    case my  = "owner"
    
    mutating func changeState() {
        if self == .all {
            self = .my
        } else {
            self = .all
        }
    }
}

class PostsViewController: UIViewController {
    
    private let tableView = UITableView(frame: CGRect.zero, style: .grouped)
    private let allPostsButton = UIButton(type: .roundedRect)
    private let myPostsButton = UIButton(type: .roundedRect)
    private let refreshControl = UIRefreshControl()
    
    private var viewModel: PostsViewModel!
    
    private var selectedPostsType: WallPostsType = .all {
        didSet {
            refreshState()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        selectedPostsType = .all
        viewModel = PostsViewModel(postsType: selectedPostsType, delegate: self)
        
        setUI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        view.layoutIfNeeded()
        allPostsButton.cornerRadius = allPostsButton.frame.height/2
        myPostsButton.cornerRadius = myPostsButton.frame.height/2
    }
    
    func setUI() {
        title = "Posts"
        
        setupTableView()
        setupPostsButtons()
    }
    
    private func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.separatorStyle = .none
        tableView.register(UINib(nibName: "PostTableViewCell", bundle: nil), forCellReuseIdentifier: CellsIdentifiers().postsTableView)
        
        tableView.refreshControl = refreshControl
        tableView.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        
        view.pinSubviewAnchors(add: true, subview: tableView)
    }
    
    private func setupPostsButtons() {
        allPostsButton.setTitle("All Posts", for: .normal)
        myPostsButton.setTitle("My Posts", for: .normal)
        
        allPostsButton.setTitleColor(UIColor.systemGray, for: .normal)
        myPostsButton.setTitleColor(UIColor.systemGray, for: .normal)
        
        allPostsButton.addTarget(self, action: #selector(allPostsButtonTapped), for: .touchUpInside)
        myPostsButton.addTarget(self, action: #selector(myPostsButtonTapped), for: .touchUpInside)
    }
    
    private func refreshState() {
        if selectedPostsType == .all {
            allPostsButton.backgroundColor = UIColor.systemGray5
            myPostsButton.backgroundColor = UIColor.white
        } else {
            allPostsButton.backgroundColor = UIColor.white
            myPostsButton.backgroundColor = UIColor.systemGray5
        }
    }
    
    @objc private func allPostsButtonTapped() {
        selectedPostsType.changeState()
        viewModel.changePostsType()
        tableView.reloadData()
    }
    
    @objc private func myPostsButtonTapped() {
        selectedPostsType.changeState()
        viewModel.changePostsType()
        tableView.reloadData()
    }
    
    @objc private func refreshData() {
        viewModel.refreshData()
        tableView.reloadData()
    }
}

extension PostsViewController: PostsViewModelDelegate {
    func refreshTableView() {
        tableView.reloadData()
        self.refreshControl.endRefreshing()
    }
}

extension PostsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.cellsNumber
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellsIdentifiers().postsTableView, for: indexPath) as? PostTableViewCell else {
            return UITableViewCell()
        }
        
        if (indexPath.row == viewModel.cellsNumber - 5) {
            viewModel.downloadNext()
        }
        
        cell.viewModel = self.viewModel.generatePostsCellViewModel(for: indexPath.row)
        
        return cell
    }
}

extension PostsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 0))
        headerView.backgroundColor = UIColor.white
        
        let buttonsStackView = UIStackView(arrangedSubviews: [allPostsButton, myPostsButton])
        buttonsStackView.translatesAutoresizingMaskIntoConstraints = false
        buttonsStackView.axis = .horizontal
        buttonsStackView.alignment = .center
        buttonsStackView.distribution = .fillProportionally
        buttonsStackView.spacing = 3
        
        headerView.addSubview(buttonsStackView)
        
        NSLayoutConstraint.activate([
            buttonsStackView.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 3),
            buttonsStackView.topAnchor.constraint(equalTo: headerView.topAnchor, constant: -3),
            buttonsStackView.bottomAnchor.constraint(equalTo: headerView.bottomAnchor, constant: 5),
            buttonsStackView.widthAnchor.constraint(equalTo: headerView.widthAnchor, multiplier: 0.5)
        ])
        return headerView
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
}

