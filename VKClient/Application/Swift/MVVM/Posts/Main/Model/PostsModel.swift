import Foundation

struct PostsModel: Codable {
    let count:       Int
    var items:      [PostModel]
    let profiles:   [WallProfileModel]
    let groups:     [GroupModel]
}

struct OwnerModel {
    let name:     String
    let imageUrl: String
}
