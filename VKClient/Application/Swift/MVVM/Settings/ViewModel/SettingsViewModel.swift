import Foundation

protocol SettingsViewModelDelegate: class {
    func updateNameLabel(with name: String)
    func updateUserPhoto(with image: Data)
}

class SettingsViewModel {
    
    let loadingQueue = OperationQueue()
    
    private var user: UserModel! {
        didSet {
            userFullName = user.first_name + " " + user.last_name
            photoURL = user.photo_100
        }
    }
    
    private var userFullName: String!
    
    private var photoURL: String!
    
    private var vkDefaults = VKDefaults.shared
    
    private weak var delegate: SettingsViewModelDelegate!
    
    init(delegate: SettingsViewModelDelegate) {
        self.delegate = delegate
        
        let secondOperation = BlockOperation {
            NetworkingManager.shared.getPicture(by: self.photoURL) { (imageData, error) in
                guard error == nil else { return }
                guard let unwrappedImageData = imageData else { return }
                self.delegate.updateUserPhoto(with: unwrappedImageData)
            }
        }
        
        let firstOperation = BlockOperation {
            NetworkingManager.shared.getUser { [weak self] (data, error) in
                guard error == nil else { return }
                guard data  != nil else { return }
                self?.user = data
                self?.delegate.updateNameLabel(with: self?.userFullName ?? "")
                secondOperation.start()
            }
        }
    
        firstOperation.start()
    }
    
    func logout() {
        NetworkingManager.shared.logout()
        vkDefaults.logout()
    }
}
