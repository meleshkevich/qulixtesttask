import Foundation

struct UserModel: Codable {
    let first_name:        String
    let id:                Int
    let last_name:         String
    let photo_100:         String
}
