import UIKit
import SVProgressHUD

class SettingsViewController: UIViewController {
    
    private let logoutButton = UIButton(type: .roundedRect)
    private let profileIcon = UIImageView()
    private let userNameLabel = UILabel()
    private let blurEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffect.Style.dark))
    
    private var viewModel: SettingsViewModel!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        blurView()
        SVProgressHUD.show()
        viewModel = SettingsViewModel(delegate: self)
        setUI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        view.layoutIfNeeded()
        
        profileIcon.cornerRadius = profileIcon.frame.height/2
    }
    
    private func setUI() {
        title = "Settings"
        
        configureLogoutButton()
        configureProfileIcon()
        configureUserNameLabel()
    }
    
    private func configureLogoutButton() {
        let logoutButtonHeight: CGFloat = 60.0
        
        logoutButton.setTitle("Log Out", for: .normal)
        logoutButton.setTitleColor(UIColor.white, for: .normal)
        logoutButton.backgroundColor = UIConstants.vkUIPatternColor
        logoutButton.translatesAutoresizingMaskIntoConstraints = false
        logoutButton.cornerRadius = logoutButtonHeight/10
        logoutButton.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        
        logoutButton.addTarget(self, action: #selector(logoutButtonTapped), for: .touchUpInside)
        
        view.addSubview(logoutButton)
        
        NSLayoutConstraint.activate([
            logoutButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -20),
            logoutButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            logoutButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            logoutButton.heightAnchor.constraint(equalToConstant: logoutButtonHeight)
        ])
    }
    
    private func configureProfileIcon() {
        let multiplier: CGFloat = 2
        
        profileIcon.translatesAutoresizingMaskIntoConstraints = false
        profileIcon.backgroundColor = UIConstants.vkUIPatternColor
        profileIcon.clipsToBounds = true
        
        view.addSubview(profileIcon)
        
        NSLayoutConstraint.activate([
            profileIcon.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20),
            profileIcon.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            profileIcon.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: multiplier/10),
            profileIcon.heightAnchor.constraint(equalTo: profileIcon.widthAnchor)
        ])
    }
    
    private func configureUserNameLabel() {
        userNameLabel.translatesAutoresizingMaskIntoConstraints = false
        userNameLabel.numberOfLines = 1
        userNameLabel.textColor = UIColor.black
        userNameLabel.font = UIFont.systemFont(ofSize: 21, weight: .semibold)
        
        view.addSubview(userNameLabel)
        
        NSLayoutConstraint.activate([
            userNameLabel.centerYAnchor.constraint(equalTo: profileIcon.centerYAnchor),
            userNameLabel.leadingAnchor.constraint(equalTo: profileIcon.trailingAnchor, constant: 20),
            userNameLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
            userNameLabel.heightAnchor.constraint(equalTo: profileIcon.heightAnchor, multiplier: 0.4)
        ])
    }
    
    private func presentLogoutAlert() {
        let alert = UIAlertController(title: "Token and user id was deleted", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { [weak self] (_) in
            let logoutWebViewController = UINavigationController(rootViewController: LogoutWebViewController())
            logoutWebViewController.modalPresentationStyle = .overFullScreen
            self?.present(logoutWebViewController, animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func blurView() {
        guard let window = ((UIApplication.shared.delegate) as! AppDelegate).window else { return }
        blurEffectView.frame = window.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        window.addSubview(blurEffectView)
    }
    
    private func unblurView() {
        blurEffectView.removeFromSuperview()
    }
    
    @objc func logoutButtonTapped() {
        viewModel.logout()
        presentLogoutAlert()
    }
}

extension SettingsViewController: SettingsViewModelDelegate {
    func updateNameLabel(with name: String) {
        userNameLabel.text = name
    }
    
    func updateUserPhoto(with imageData: Data) {
        guard let uiImage = UIImage(data: imageData) else { return }
        profileIcon.image = uiImage
        
        unblurView()
        SVProgressHUD.dismiss()
    }
}
