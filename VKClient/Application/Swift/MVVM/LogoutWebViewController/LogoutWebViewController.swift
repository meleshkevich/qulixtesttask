import UIKit
import WebKit

class LogoutWebViewController: UIViewController {
    
    private let webView = WKWebView(frame: .zero, configuration: WKWebViewConfiguration())
    private let authorizationConstants = VKAuthorizationEndpoint()
    private let progressView = UIProgressView(progressViewStyle: .default)
    
    override func loadView() {
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setToolbar()
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
        setNavigationItem()
        
        startLogout()
    }
    
    private func startLogout() {
        let logoutStringUrl = authorizationConstants.userApps
        guard let authorizationURL = URL(string: logoutStringUrl) else { return }
        
        webView.load(URLRequest(url: authorizationURL))
    }
    
    func setNavigationItem() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(goToLogoutScreen))
    }
    
    func setToolbar() {
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let refreshButton = UIBarButtonItem(barButtonSystemItem: .refresh, target: webView, action: #selector(webView.reload))
        progressView.sizeToFit()
        let progressButton = UIBarButtonItem(customView: progressView)
        toolbarItems = [progressButton, space, refreshButton]
        self.navigationController?.isToolbarHidden = false
    }
    
    @objc private func goToLogoutScreen() {
        NotificationCenter.default.post(Notification(name: Notification.Name.logout))
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            self.progressView.progress = Float(webView.estimatedProgress)
        }
    }
}
