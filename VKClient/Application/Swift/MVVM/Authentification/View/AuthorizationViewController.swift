import UIKit
import WebKit

class AuthorizationViewController: UIViewController {
    
    private let webView = WKWebView(frame: .zero, configuration: WKWebViewConfiguration())
    private let authorizationConstants = VKAuthorizationEndpoint()
    
    private var vkDefaults = VKDefaults.shared
    
    override func loadView() {
        view = webView
        webView.navigationDelegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        startAuthorization()
    }
    
    private func startAuthorization() {
        let authorizationStringUrl = authorizationConstants.authorizeStringURL
        guard let authorizationURL = URL(string: authorizationStringUrl) else { return }
        
        webView.load(URLRequest(url: authorizationURL))
    }
    
    func processURL(_ url: URL) {
        let stringURL = url.absoluteString
        var parametsString: String!
        
        do {
            parametsString = try stringURL.deletePrefix(authorizationConstants.redirectDroppingStringURL)
        } catch {
            return
        }
        
        processParametrs(parametsString)
    }
    
    func processParametrs(_ parametrsString: String) {
        var parametrs = ["", "", ""]
        
        var counter = 0
        var adding = true
        
        for element in parametrsString {
            if !adding {
                if element != "=" {
                    continue
                }
                
                adding = true
                counter += 1
                continue
            }
            
            if element == "&" {
                adding = false
            } else {
                parametrs[counter].append(element)
            }
        }
        
        vkDefaults.setValues(token: parametrs[0], id: parametrs[2], expiration: parametrs[1])
        
    }
}

extension AuthorizationViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        guard let url = webView.url else { return }
        processURL(url)
    }
}
