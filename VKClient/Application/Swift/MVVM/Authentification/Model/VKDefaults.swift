import Foundation

class VKDefaults {
    static let shared = VKDefaults()
    
    private init() {}
    
    @UserDefault(key: UserDefaultsKeys.accessToken.rawValue, initialValue: "")
    var accessToken: String
    @UserDefault(key: UserDefaultsKeys.userID.rawValue, initialValue: "")
    var userID: String
    @UserDefault(key: UserDefaultsKeys.expirationDate.rawValue, initialValue: "")
    var expirationDate: String
    
    var paramsDidFetched: Bool {
        guard (!accessToken.isEmpty), (!userID.isEmpty), (!expirationDate.isEmpty) else { return false }
        return true
    }
    
    func logout() {
        UserDefaults.standard.removeObject(forKey: UserDefaultsKeys.accessToken.rawValue)
        UserDefaults.standard.removeObject(forKey: UserDefaultsKeys.userID.rawValue)
        UserDefaults.standard.removeObject(forKey: UserDefaultsKeys.expirationDate.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    func setValues(token: String, id: String, expiration: String) {
        accessToken = token
        userID = id
        var currentDate = DateComparer.currentDate
        // to fix
        currentDate?.2 += 1
        expirationDate = "\(currentDate!.0)-\(currentDate!.1)-\(currentDate!.2)T\(currentDate!.3):\(currentDate!.4)"
        
        NotificationCenter.default.post(Notification(name: Notification.Name.tokenDidFetched))
        UserDefaults.standard.synchronize()
    }
}
