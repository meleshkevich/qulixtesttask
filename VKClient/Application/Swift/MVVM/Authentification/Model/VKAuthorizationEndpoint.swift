struct VKAuthorizationEndpoint {
    let appID = VKBasicConstants.appID
    let apiVersion = VKBasicConstants.apiVersion
    let baseUrl = "https://oauth.vk.com/"
    let userApps = "https://vk.com/settings?act=apps"
    
    var redirectDroppingStringURL: String {
        return baseUrl + "blank.html#access_token="
    }
    
    var authorizeStringURL: String {
        return "\(baseUrl)authorize?client_id=\(appID)&display=page&redirect_uri=https://oauth.vk.com/blank.html&scope=friends&response_type=token&v=\(apiVersion)"
    }
}
